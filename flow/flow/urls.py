"""flow URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

import portal.views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', portal.views.index, name='index'),
    path('water', portal.views.water, name='water'),
    path('suggestions/', portal.views.suggestions, name='suggestions'),
    path('trivia/', portal.views.trivia, name='trivia'),
    path('consumption/', portal.views.consumption, name='consumption'),
    path('users/', portal.views.users, name='users'),
    path('personal/<str:id>/', portal.views.personal, name='personal'),
]
