from django.db import models
from django.utils.timezone import now
import django


class Post(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField(default="This is the body")
    created_at = models.DateTimeField(default=django.utils.timezone.now)
    updated_at = models.DateTimeField(default=django.utils.timezone.now)

    def __repr__(self):
        return str(self.title) + " - " + str(self.body)


class Consumption(models.Model):
    user = models.CharField(max_length=50)
    usage = models.CharField(max_length=50)
    created_at = models.DateTimeField(default=django.utils.timezone.now)

    def __str__(self):
        return str(self.user) + ": " + self.usage


class Suggestion(models.Model):
    text = models.CharField(max_length=300)
    trigger = models.IntegerField(default=0)

    def __str__(self):
        return str(self.text) + str(self.trigger)


class Trivia(models.Model):
    text = models.CharField(max_length=300)

    def __str__(self):
        return self.text


class User(models.Model):
    rfid = models.CharField(max_length=50)
    credits = models.IntegerField(default=0)

    def __str__(self):
        return str(self.rfid) + ": " + str(self.credits)
