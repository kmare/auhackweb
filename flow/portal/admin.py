from django.contrib import admin

from portal.models import Post, Consumption, Suggestion, Trivia, User

admin.site.register(Post)
admin.site.register(Consumption)
admin.site.register(Suggestion)
admin.site.register(Trivia)
admin.site.register(User)
