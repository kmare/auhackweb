from django.db.models import F
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
import datetime
import random

from portal.models import Post, Consumption, Suggestion, Trivia, User


def index(request):
    title = "The title's portal!"
    posts = Post.objects

    today = datetime.date.today() + datetime.timedelta(days=1)
    yesterday = datetime.date.today() - datetime.timedelta(days=1)
    weekago = datetime.date.today() - datetime.timedelta(days=7)
    monthago = datetime.date.today() - datetime.timedelta(days=30)

    con = Consumption.objects.filter(created_at__range=(yesterday, today))
    u = Consumption.objects.filter(created_at__range=(yesterday, today)).values_list('user', flat=True)
    us = Consumption.objects.filter(created_at__range=(yesterday, today)).values_list('usage', flat=True)
    u = list(map(str, u))
    us = list(map(int, us))

    uw = Consumption.objects.filter(created_at__range=(weekago, today)).values_list('user', flat=True)
    usw = Consumption.objects.filter(created_at__range=(weekago, today)).values_list('usage', flat=True)
    uw = list(map(str, uw))
    usw = list(map(int, usw))

    um = Consumption.objects.filter(created_at__range=(monthago, today)).values_list('user', flat=True)
    usm = Consumption.objects.filter(created_at__range=(monthago, today)).values_list('usage', flat=True)
    um = list(map(str, um))
    usm = list(map(int, usm))

    um1 = User.objects.values_list('rfid', flat=True)
    usm1 = User.objects.values_list('credits', flat=True)
    um1 = list(map(str, um1))
    usm1 = list(map(int, usm1))

    return render(request, 'index.html', {'title': title, 'posts': posts, 'consumptions': con, 'users': u, 'usage': us,
                                          'usersw': uw, 'usagew': usw, 'usersm': um, 'usagem': usm, 'rfid': um1, 'credits': usm1})


@csrf_exempt
def water(request):
    if request.method == 'POST':
        if request.POST['user'] is not None and request.POST['usage'] is not None :
            consumption = Consumption.objects.create(user=request.POST['user'], usage=request.POST['usage'])
            cr = random.randint(-5, 5)
            creds = map_credits(int(request.POST['usage']), 0, 50, -5, 5)
            print(str(creds))
            user = User.objects.filter(rfid=request.POST['user']).update(credits=F('credits') + creds)
            return HttpResponse("success usage!")

    return HttpResponse("failed!")


def users(request):
    us = User.objects.all

    um = User.objects.values_list('rfid', flat=True)
    usm = User.objects.values_list('credits', flat=True)
    um = list(map(str, um))
    usm = list(map(int, usm))
    return render(request, 'users.html', {'users': us, 'rfid': um, 'credits': usm})


def suggestions(request):
    sugg = Suggestion.objects
    return render(request, 'suggestions.html', {'suggestions': sugg})


def trivia(request):
    tr = Trivia.objects
    return render(request, 'trivia.html', {'trivia': tr})


def consumption(request):
    today = datetime.date.today() + datetime.timedelta(days=1)
    yesterday = datetime.date.today() - datetime.timedelta(days=1)
    weekago = datetime.date.today() - datetime.timedelta(days=7)
    monthago = datetime.date.today() - datetime.timedelta(days=30)

    con = Consumption.objects.filter(created_at__range=(yesterday, today))
    u = Consumption.objects.filter(created_at__range=(yesterday, today)).values_list('user', flat=True)
    us = Consumption.objects.filter(created_at__range=(yesterday, today)).values_list('usage', flat=True)
    u = list(map(str, u))
    us = list(map(int, us))

    uw = Consumption.objects.filter(created_at__range=(weekago, today)).values_list('user', flat=True)
    usw = Consumption.objects.filter(created_at__range=(weekago, today)).values_list('usage', flat=True)
    uw = list(map(str, uw))
    usw = list(map(int, usw))

    um = Consumption.objects.filter(created_at__range=(monthago, today)).values_list('user', flat=True)
    usm = Consumption.objects.filter(created_at__range=(monthago, today)).values_list('usage', flat=True)
    um = list(map(str, um))
    usm = list(map(int, usm))

    return render(request, 'consumption.html', {'consumptions': con, 'users': u, 'usage': us,
                                                'usersw': uw, 'usagew': usw, 'usersm': um, 'usagem': usm})


def personal(request, id):  # 7609666
    rfid = User.objects.filter(rfid=id)
    rfid = rfid[0]
    print(id)
    # print(rfid.all)

    tr = Trivia.objects
    triv = Trivia.objects.order_by('?')[:1]
    return render(request, 'personal.html', {'trivia': triv, 'user': rfid})


def map_credits(x, a, b, c, d):
    y = (x-a)/(b-a)*(d-c)+c
    return round(y)
