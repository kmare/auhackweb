# Generated by Django 2.2 on 2019-04-06 21:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0005_suggestion_trivia'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rfid', models.CharField(max_length=50)),
                ('credits', models.IntegerField(default=0)),
            ],
        ),
    ]
